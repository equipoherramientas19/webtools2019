function Revisar(){
    var fraseMoti = document.getElementById('frase-mot').value;

                /*********INCISO A) ********/
    var fraseMit=' ';
    var tamMit = parseInt((fraseMoti.length)/2);
    for(var i=0;i<tamMit;i++){
        fraseMit = fraseMit + fraseMoti.charAt(i);
    }

    var iframedocA = revisarA.document;

    if(revisarA.contentDocument){
        iframedocA = revisarA.contentDocument;
    }else{
        if (revisarA.contentWindow){
            iframedocA = revisarA.contentWindow.document;
        }
    }

    if(iframedocA){
        iframedocA.open();
        iframedocA.writeln(fraseMit);
        iframedocA.close();
    }else{
        alert('No es posible insertar el contenido dinámicamente en el iframe.');
    }

                /*********INCISO B) Imprimir el último carácter. ********/
     var caracterU = fraseMoti.charAt((fraseMoti.length)-1);

    var iframedocB = revisarB.document;

    if(revisarB.contentDocument){
        iframedocB = revisarB.contentDocument;
    }else{
        if (revisarB.contentWindow){
            iframedocB = revisarB.contentWindow.document;
        }
    }

    if(iframedocB){
        iframedocB.open();
        iframedocB.writeln(caracterU);
        iframedocB.close();
    }else{
        alert('No es posible insertar el contenido dinámicamente en el iframe.');
    }

                /*********INCISO C) Imprimirlo en forma inversa.********/
    var formaInv= ' ';
    var tam=fraseMoti.length;
    for(var i=tam-1;i>=0;i--){
        formaInv=formaInv+fraseMoti.charAt(i);
    }

    var iframedocC=revisarC.document;

    if(revisarC.contentDocument){
        iframedocC=revisarC.contentDocument;
    }else{
        if (revisarC.contentWindow){
            iframedocC=revisarC.contentWindow.document;
        }
    }

    if(iframedocC){
        iframedocC.open();
        iframedocC.writeln(formaInv);
        iframedocC.close();
    }else{
        alert('No es posible insertar el contenido dinámicamente en el iframe.');
    }

                /*********INCISO D) Imprimir cada carácter del string separado con un punto. ********/
    var puntoSep=' ';
    for(var i=0;i<tam;i++)
    {
        puntoSep = puntoSep + fraseMoti.charAt(i) +'.';
    }

    var iframedocD = revisarD.document;

    if(revisarD.contentDocument){
        iframedocD = revisarD.contentDocument;
    }else{
        if (revisarD.contentWindow){
            iframedocD=revisarD.contentWindow.document;
        }
    }

    if(iframedocD){
        iframedocD.open();
        iframedocD.writeln(puntoSep);
        iframedocD.close();
    }else{
        alert('No es posible insertar el contenido dinámicamente en el iframe.');
    }

                /*********INCISO E) Imprimir la cantidad de vocales ‘a’ almacenadas. ********/
    var cont=0;
    for(var i=0;i<tam;i++)
    {
        if((fraseMoti.charAt(i)=='a')||(fraseMoti.charAt(i)=='A'))
        {
            cont++;
        }
    }

    var iframedocE=revisarE.document;

    if(revisarE.contentDocument){
        iframedocE=revisarE.contentDocument;
    }else{
        if (revisarE.contentWindow){
            iframedocE=revisarE.contentWindow.document;
        }
    }

    if(iframedocE){
        iframedocE.open();
        iframedocE.writeln(cont);
        iframedocE.close();
    }else{
        alert('No es posible insertar el contenido dinámicamente en el iframe.');
    }
}