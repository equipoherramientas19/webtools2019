Datos Licenciatura

Benemérita Universidad Autónoma de Puebla
Facultad de Ciencias de la Computación
Dirección: Av. San Claudio, Blvrd 14 Sur, Cdad. Universitaria, 72592 Puebla, Pue.
Carrera: Ingenieria en Tecnologias de la Información
Modalidad: Presencial
Duración del Plan:
 a) Dedicación en horas: 
   *Horas Mínimas:4514
   *Horas Máximas: 4910
 b)Tiempo:
   *Mínimo: 4.5 años
   *Máximo: 6.5 años
 c)Créditos: 
   *Mínimos: 257
   *Máximos: 284
Tipo de Plan de Estudios: Científico-Práctico
Título que se otorga: Licenciado(a) en Ingeniería en Tecnologías
de la Información.
Ceritificado que se otorga: Ingeniero (a) en Tecnologías de la Información